const mongoose = require('mongoose');
const config = require('../../config');

async function connect () {
    try {
        await mongoose.connect(config.DB);
        console.log("Connect to DB mongoDB successfully!");
    } catch (error) {
        console.error("Connection failed, please try!", error);
    }
    
}

module.exports = { connect };
