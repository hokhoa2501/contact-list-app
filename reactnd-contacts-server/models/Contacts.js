const mongoose = require('mongoose');
const { Schema } = mongoose;

const contactsSchema = new Schema({
    id: String,
    name: String,
    email: String,
    avatarURL: String,
});

module.exports = mongoose.model('Contacts', contactsSchema);