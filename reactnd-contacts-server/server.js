const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const config = require('./config')
const Contacts = require('./models/Contacts');

const multiMogoToObject = (mongooses) => {
  return mongooses.map(mongoose => mongoose.toObject());
}

const app = express()

app.use(express.static('public'))
app.use(cors())
//Connect DB
const db = require('./config/db');
db.connect();

app.get('/', (req, res) => {
  const help = `
  <pre>
    Welcome to the Address Book API!

    Use an Authorization header to work with your own data:

    fetch(url, { headers: { 'Authorization': 'whatever-you-want' }})

    The following endpoints are available:

    GET /contacts
    DELETE /contacts/:id
    POST /contacts { name, email, avatarURL }
  </pre>
  `

  res.send(help)
})

app.use((req, res, next) => {
  const token = req.get('Authorization')

  if (token) {
    req.token = token
    next()
  } else {
    res.status(403).send({
      error: 'Please provide an Authorization header to identify yourself (can be whatever you want)'
    })
  }
})

app.get('/contacts', (req, res, next) => {
  // res.send(contacts.get(req.token))
  Contacts.find({})
    .then(Contacts => res.send({ contacts: multiMogoToObject(Contacts) }))
    .catch(next);
})

app.delete('/contacts/:id', (req, res) => {
  // res.send(contacts.remove(req.token, req.params.id))
  Contacts.findByIdAndRemove(req.params.id, function (err, data) {
    if (data) {
      res.json({ status: "Delete contact id: " + req.params.id + " success" })
    } else {
      res.json({ status: "Delete contact id: " + req.params.id + " failure" })
    }
  });
})

app.post('/editContacts', bodyParser.json(), (req, res) => {
  const { _id, id, name, email } = req.body
  Contacts.findByIdAndUpdate(_id, { name, email }, (err, contact) => {
    if (!err) {
        res.json({ status: "Edit contact id: " + id + " success" })
    } else {
        res.json({ status: "Edit contact id: " + id + " failure" })
    }
});
})

app.post('/contacts', bodyParser.json(), (req, res) => {
  const { name, email, avatarURL } = req.body

  if (name && email) {
    // res.send(contacts.add(req.token, req.body))
    const id = Math.random().toString(36).substr(-8)
    const contact = new Contacts({ id, name, email, avatarURL });
    contact.save((err, doc) => {
      if (!err) res.json({ name, email, avatarURL })
      else res.json({ status: "failure" })
    })
  } else {
    res.status(403).send({
      error: 'Please provide both a name and email address'
    })
  }
})

app.post('/importContacts', bodyParser.json(), (req, res) => {
  if (req.body) {
    // res.send(contacts.addMany(req.token, req.body))
    const contacts = req.body
    if (contacts.length > 0) {
      for (let contact of contacts) {
        if (!contact.id) {
          contact.id = Math.random().toString(36).substr(-8)
        }
        const contactDB = new Contacts(contact);
        try {
          contactDB.save((err, doc) => {
          })
        } catch (error) {
          console.error("err ", error)
        }
      }
      res.json({})
    }
  } else {
    res.status(403).send({
      error: 'Please provide CSV file format'
    })
  }
})

app.listen(config.port, () => {
  console.log('Server listening on port %s, Ctrl+C to stop', config.port)
})
