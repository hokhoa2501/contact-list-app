import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import escapeRegExp from 'escape-string-regexp'
import sortBy from 'sort-by'


// Components, class method

class ListContacts extends Component {
  static propTypes = {
    contacts: PropTypes.array.isRequired,
    onDeleteContact: PropTypes.func.isRequired,
    onEditContact: PropTypes.func.isRequired
  }

  state = {
    query: '',
    editID:'',
    name: '',
    email:''
  }

  updateQuery = (query) => {
    this.setState({ query: query.trim() })
  }

  clearQuery = () => {
    this.setState({ query: '' })
  }

  handleEditContact = (contact,onEditContact) => {
    onEditContact(contact)
    this.setState({ editID: contact.id, name: contact.name, email: contact.email })
  }

  handleUpdateContact = (contact,onUpdateContact) => {
    let contactChanged = {... contact, name: this.state.name, email: this.state.email}
    onUpdateContact(contactChanged)
    this.setState({ editID: '', name: '', email: '' })
  }

  handleChangeName = (e) => {
    this.setState({ name: e.target.value });
  };

  handleChangeEmail = (e) => {
    this.setState({ email: e.target.value });
  };

  render() {
    const { contacts, onDeleteContact, onEditContact, isEdit, onUpdateContact } = this.props
    const { query } = this.state
    let showingContacts

    if (query) {
      const match = new RegExp(escapeRegExp(query), 'i')
      showingContacts = contacts.filter((contact) => match.test(contact.name))
    } else {
      showingContacts = contacts
    }

    showingContacts.sort(sortBy('name'))

    return (
      <div className='list-contacts'>
        <h3 className='page-title'>Contacts Directory</h3>
        <a className='download-template' href='example.csv'>Download template</a>
        <div className='list-contacts-top'>
          <input
            className='search-contacts'
            type='text'
            placeholder='Search contacts..'
            value={query}
            onChange={(event) => this.updateQuery(event.target.value)}/>
            <Link
              to='/create'
              className='add-contact'>
              Add Contact</Link>
        </div>

        {showingContacts.length !== contacts.length && (
          <div className='showing-contacts'>
            <span>{showingContacts.length} of {contacts.length} | </span>
            <button onClick={this.clearQuery}>Show All</button>
          </div>
        )}

        <ol className='contact-list'>
          {showingContacts.map( contact => (
            <li key={contact.id} className='contact-list-item'>
              <div className='contact-avatar' style={{
                backgroundImage: `url(${contact.avatarURL})`
              }}> </div>
              {this.state.editID != contact.id && <div className='contact-details'>
                <p>{contact.name}</p>
                <p>{contact.email}</p>
              </div>}
              {isEdit && this.state.editID === contact.id && <div className='contact-details'>
                <input className='contact-details-input' type='text' placeholder='Name' name='name' value={this.state.name} onChange={this.handleChangeName} />
                <input className='contact-details-input' type='text' placeholder='Email' name='email' value={this.state.email} onChange={this.handleChangeEmail} />
                <button className='contact-details-button' onClick={() => this.handleUpdateContact(contact,onUpdateContact)}>Update</button>
              </div>}
              <button className='contact-edit' onClick={() => this.handleEditContact(contact, onEditContact)}>
                Edit
              </button>
              <button className='contact-remove' onClick={() => onDeleteContact(contact)}>
                Remove
              </button>
            </li>
          ))}
        </ol>
      </div>
    )
  }
}


// Functional components or Stateless functional component
// for 'read-only' immutable data
// since it only do one task of rendering and returning html

// function ListContacts(props) {
//   return (
//     <ol className='contact-list'>
//       {props.contacts.map( contact => (
//         <li key={contact.id} className='contact-list-item'>
//           <div className='contact-avatar' style={{
//             backgroundImage: `url(${contact.avatarURL})`
//           }}> </div>
//           <div className='contact-details'>
//             <p>{contact.name}</p>
//             <p>{contact.email}</p>
//           </div>
//           <button className='contact-remove' onClick={() => props.onDeleteContact(contact)}>
//             Remove
//           </button>
//         </li>
//       ))}
//     </ol>
//   )
// }





export default ListContacts
