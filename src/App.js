import React, { Component } from 'react';
import ListContacts from './ListContacts'
import CreateContact from './CreateContact'
import CSVReader from './importCSV'
import { Route } from 'react-router-dom'
import * as ContactsAPI from './utils/ContactsAPI'

// class components for mmutable data to track any changes in the state

class App extends Component {
  state = {
    contacts: [],
    isEdit: false
  }

  componentDidMount() {
    ContactsAPI.getAll().then((contacts) => {
      this.setState({ contacts })
    })
  }

  removeContact = async (contact) => {
    const res = await ContactsAPI.remove(contact)
    ContactsAPI.getAll().then((contacts) => {
      this.setState({ contacts })
    })
  }

  editContact = (contact) => {
    this.setState(state =>({
      isEdit: true
    }))
  }

  updateContact = async (contact) => {
    const resEdit = await ContactsAPI.edit(contact)
    ContactsAPI.getAll().then((contacts) => {
      this.setState({ contacts })
    })
  }

  async createContact(contact) {
    const res = await ContactsAPI.create(contact)
    ContactsAPI.getAll().then((contacts) => {
      this.setState({ contacts })
    })
  }

  async importContact(listArray) {
    let request = []
    for (let i = 1; i < listArray.length - 1; i++) {
      const array = listArray[i]
      let contact = {}
      for (let j = 0; j < array.length; j++) {
        switch (j) {
          case 0:
            contact.avatarURL = array[0]
            break
          case 1:
            contact.id = array[1]
            break
          case 2:
            contact.name = array[2]
            break
          default:
            contact.email = array[3]
            break
        }
      }
      request.push(contact)
    }
    const resImport = await ContactsAPI.importCSV(request)
    ContactsAPI.getAll().then((contacts) => {
      this.setState({ contacts })
    })
  }

  render() {
    return (
      <div className='app'>
        <Route exact path='/' render={() => (
          <div>
            <ListContacts
              contacts={this.state.contacts}
              onDeleteContact={this.removeContact}
              onEditContact={this.editContact}
              onUpdateContact={this.updateContact}
              isEdit={this.state.isEdit}
            />
            <CSVReader onImportContact={(contacts) => {
              this.importContact(contacts)
            }} />
          </div>
        )} />

        <Route path='/create' render={({ history }) => (
          <CreateContact
            onCreateContact={(contact) => {
              this.createContact(contact)
              history.push('/')
            }}
          />
        )} />
      </div>
    )
  }
}

export default App;
